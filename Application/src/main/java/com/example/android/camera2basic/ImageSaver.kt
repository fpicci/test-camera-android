package com.example.android.camera2basic

import android.media.Image
import android.util.Base64
import android.util.Log
import com.android.volley.AuthFailureError
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.StringRequest
import java.io.File
import java.io.FileOutputStream
import java.io.IOException

/**
 * Saves a JPEG [Image] into the specified [File].
 */
internal class ImageSaver(
        /**
         * The JPEG image
         */
        private val image: Image,

        /**
         * The file we save the image into.
         */
        private val file: File
) : Runnable {

    override fun run() {
        val buffer = image.planes[0].buffer
        val bytes = ByteArray(buffer.remaining())
        buffer.get(bytes)
        var output: FileOutputStream? = null
        try {
            output = FileOutputStream(file).apply {
                write(bytes)
            }
        } catch (e: IOException) {
            Log.e(TAG, e.toString())
        } finally {
            image.close()
            upload(file)

            output?.let {
                try {
                    it.close()
                } catch (e: IOException) {
                    Log.e(TAG, e.toString())
                }

            }
        }
    }

    fun upload(image: File) {
        Log.i(TAG, "start upload")
        val uploadRequest = object : StringRequest(Request.Method.POST,
                "https://api.imgur.com/3/image",
                object : Response.Listener<String> {
            override fun onResponse(response: String) {
                Log.d(TAG, "finished image upload")
            }
        }, object : Response.ErrorListener {
            override fun onErrorResponse(error: VolleyError) {
                Log.e(TAG, error.toString())
                Log.e(TAG, "finish/error upload")
            }
        }) {
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers.put("Authorization", "Client-ID b6487319a02f58d")
                headers.put("content-type", "multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW")
                return headers
            }

            @Throws(AuthFailureError::class)
            override fun getParams(): MutableMap<String, String>? {
                val params = HashMap<String,String>()
                params.put("image", encoder(image.absolutePath))
                params.put("title", "")
                params.put("type", "base64")
                params.put("name", "myimage")
                return params
            }
        }
        MainApplication.requestQueue!!.add(uploadRequest)
    }

    fun encoder(filePath: String): String{
        val bytes = File(filePath).readBytes()
        return Base64.encodeToString(bytes, Base64.DEFAULT)
    }

    companion object {
        /**
         * Tag for the [Log].
         */
        private val TAG = "ImageSaver"
    }
}
