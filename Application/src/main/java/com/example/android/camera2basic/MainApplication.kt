package com.example.android.camera2basic

import android.app.Application
import android.content.Context
import com.android.volley.DefaultRetryPolicy
import com.android.volley.RequestQueue
import com.android.volley.toolbox.Volley

class MainApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        MainApplication.applicationContext = applicationContext
        requestQueue = Volley.newRequestQueue(applicationContext)
        DefaultRetryPolicy(10000, 0, 0f)
    }

    companion object {
        var applicationContext: Context? = null
        var requestQueue: RequestQueue? = null
            private set
        private val TAG = MainApplication::class.java.simpleName
    }
}