# Camera app

App di esempio che usa la libreria camera2 per scattare una foto e caricarla sul un album su imgur. Utilizza firebase analytics.

## Getting Started

### Prerequisites

Android studio 3.1.4, un device che supporti le api camera2, dotato di connessione ad internet.


## Built With

* [Android studio] - Ambiente di sviluppo
* [Kotlin] - Linguaggio di programmazione
* [camera2]- Ultima Api per usare la camera
* [imgur rest api] - Api per caricare le immagini
* [firebase analytics] - Framework di monitoraggio



## Authors

* **Federico Piccirilli** - *All the work*

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Grazie google per la documentazione.
